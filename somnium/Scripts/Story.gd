extends RichTextLabel

var dialogue = ["Hi, i'm XJ. I was born with a not-so-ordinary gift of being able to communicate with people from 'the other side', as my parents would say.", 
				"Well, i can't actually see and talk to them directly, but they usually come to me in my dreams to ask for a favor.",
				"They would give me 'visions' of places, usually related to their deaths. (it's really dark in there)",
				"Through those visions, they would give me 'hints' that can help me understand what they're trying to say and what i can do to help them.",
				"The hints don't come easy, though. They usually come in the form of puzzles hidden inside things that are scattered around the room i'm in.",
				"I have to find the hints and solve the puzzle in under 3.23 minutes, or else i'll be stuck in my dream forever.",
				"Why? I'm not even sure myself, but it has something to do with the forces from the other side.",
				"Sometimes i can't roam free in my dreams either, because the forces are just too strong for me to handle.",
				"This whole thing seems like such a hassle, isn't it? Well i feel like that too, sometimes. But hey, it's nice to help others even when they're not alive anymore.",
				"Enough with my life story, then. Now help me solve this one, will you?"]
var page = 0
onready var dialogue_bar = get_parent()

func _ready():
	set_bbcode(dialogue[page])
	set_visible_characters(0)
	set_process_input(true)
	
func _input(_event):
	if Input.is_key_pressed(KEY_RIGHT):
		if get_visible_characters() > get_total_character_count():
			if page < dialogue.size()-1:
				page += 1
				set_bbcode(dialogue[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())
	if Input.is_key_pressed(KEY_ENTER):
		get_tree().change_scene(str("res://Scenes/Level1.tscn")) 
	
func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
	
