extends RichTextLabel

var dialogue = ["Hmmm, a birthday party?",
				"There are quite a lot of people who came too, according to the plates.",
				"I don't think i have enough hints to solve this one, but i'm sure this present has something to do with the case.",
				"The person from the other side will come to me again in my next dream, so don't worry too much about it.",
				"Now i have to wake up so i don't get stuck here forever.",
				"See you tomorrow night!"]
var page = 0
onready var dialogue_bar = get_parent()

func _ready():
	set_bbcode(dialogue[page])
	set_visible_characters(0)
	set_process_input(true)
	
func _input(_event):
	if Input.is_key_pressed(KEY_RIGHT):
		if get_visible_characters() > get_total_character_count():
			if page < dialogue.size()-1:
				page += 1
				set_bbcode(dialogue[page])
				set_visible_characters(0)
		else:
			set_visible_characters(get_total_character_count())
	if Input.is_key_pressed(KEY_ENTER):
		get_tree().change_scene(str("res://Scenes/MainMenu.tscn")) 


func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
