extends Area2D

export var id = 1
var drag = false
	
func _process(_delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) && drag == true:
		position = get_global_mouse_position()
		
func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton && event.button_index == BUTTON_LEFT && event.pressed:
		drag = true
		Global.id = id
		Global.pos_x = position.x
		z_index = 1
	if event is InputEventMouseButton && event.button_index == BUTTON_LEFT && !event.pressed:
		drag = false
		z_index = 0
