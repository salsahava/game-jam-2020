extends Label

func _process(delta):
	if Global.s > 59:
		Global.m += 1
		Global.s = 0
	
	self.text = str(Global.m) + ":" + str(Global.s)

func _on_Timer_timeout():
	Global.s += 1
	if Global.m == 3 && Global.s == 23:
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
