extends Area2D

export var id = 1
onready var answer1 = $"../Answer1"
onready var answer2 = $"../Answer2"
onready var answer3 = $"../Answer3"

var collide = false
	
func _process(_delta):
	if collide == true && !Input.is_mouse_button_pressed(BUTTON_LEFT):
		if id == Global.id:
			Global.countRight += 1
			queue_free()
			
			if Global.countRight == 3:
				Global.countRight = 0
				get_tree().change_scene(str("res://Scenes/Solved.tscn"))
				
			match Global.id:
				1: 
					answer1.queue_free()
				2:
					answer2.queue_free()
				3:
					answer3.queue_free()
		else:
			match Global.id:
				1:
					answer1.position = Vector2(Global.pos_x, 460)
				2:
					answer2.position = Vector2(Global.pos_x, 460)
				3:
					answer3.position = Vector2(Global.pos_x, 460)
func _on_Block1_area_entered(area):
	collide = true

func _on_Block1_area_exited(area):
	collide = false
